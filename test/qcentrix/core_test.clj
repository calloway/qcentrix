(ns qcentrix.core-test
  (:require [clojure.test :refer :all]
            [qcentrix.core :refer :all]
            [qcentrix.problem1 :refer :all]))

(def expected-tree {:right {:right {:right {}, :left {}, :root "F"}, :left {}, :root "C"}, :left {:right {:right {}, :left {}, :root "E"}, :left {:right {}, :left {}, :root "D"}, :root "B"}, :root "A"})

(def inorder "DBEACF")
(def preorder "ABDECF")

(deftest problem1-test
  (testing "testing the sample tree"
    (is (= expected-tree (construct-tree preorder inorder)))))
