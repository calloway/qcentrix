(ns qcentrix.problem1
  (:require [clojure.string :refer [split]]
            [clojure.pprint :refer :all]))

;; in­order traversal : DBEACF
;; pre­order traversal : ABDECF

(defn- root-left-right-inorder 
  "Extracts the root, left and right in inorder from the given preorder and inorder. Only goes one layer"

  [preorder inorder]
  (let [root (str (first preorder))
        [left right] (split inorder (re-pattern root))]
    [root left right]))

(defn- left-right-preorder
  "Extracts the left and right in preorder from the given preorder and inorder. Only goes one layer"
  [preorder right-inorder]
  (let [not-root? #(not (= % (first right-inorder)))
        left (apply str (take-while not-root?  (rest preorder)))
        right (apply str (drop-while not-root? (rest preorder)))]
    [left right]))
    
(defn construct-tree 
  "Constructs a tree list from pre-order and in-order traversal"
  ([preorder inorder] (construct-tree preorder inorder {}))
  ([preorder inorder tree]
     (if (or (empty? preorder) (empty? inorder))
       tree
     (let [leftroot (second preorder)
           [root left-inorder right-inorder] (root-left-right-inorder preorder inorder)
           [left-preorder right-preorder] (left-right-preorder preorder right-inorder)]
       (conj tree {:root root 
                   :left (construct-tree left-preorder left-inorder)
                   :right (construct-tree right-preorder right-inorder)} )))))


(defn -main [& args]
  (println "inorder DBEACF, preorder ABDECF")
  (-> (construct-tree "ABDECF" "DBEACF")
      pprint))










